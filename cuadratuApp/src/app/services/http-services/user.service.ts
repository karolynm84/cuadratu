import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models';
import { HttpDefinitions } from './http-definitions.service';
 
@Injectable()
export class UserService {
    users: User[];

    private readonly URL = HttpDefinitions.URL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getUsers(token):  Observable<User[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<User[]>(this.URL+'users', httpOptions);                    
    }

    createUser(user) {
        console.log(user);
        let body = JSON.stringify(user);
        return this.http.post(this.URL + 'signup', body, HttpDefinitions.httpOptionsTokenSafe);
    }

/*    createUserByInvitation(user) {
        console.log(user);
        let body = JSON.stringify(user);
        return this.http.post<User>(this.URL +'user'+ '/store', body, HttpDefinitions.httpOptionsJson);
    }*/

    updateUser(user, token: string) {
        let body = JSON.stringify(user);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.put(this.URL + 'users/' + user.id, body, httpOptions);
    }

    getUser(user, token: string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<User>(this.URL + 'user/' + user, httpOptions);
    }

    deleteUser(user, token:string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + 'users/' + user, httpOptions);
    }

    sendEmailBienvenida(loginObject){
        console.log(loginObject);
        let body = JSON.stringify(loginObject);
        return this.http.post(this.URL + 'users/' +'sendemailbienvenida', loginObject, HttpDefinitions.httpOptions);
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'http://localhost:4200, http://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}