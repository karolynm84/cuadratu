import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models';
import { HttpDefinitions } from './http-definitions.service';
 
@Injectable()
export class ProductsService {
    products: Product[];

    private readonly URL = HttpDefinitions.URL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getProducts(token):  Observable<Product[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Product[]>(this.URL+'products', httpOptions);                    
    }

    addProduct(product) {
        console.log(product);
        let body = JSON.stringify(product);
        return this.http.post(this.URL + 'signup', body, HttpDefinitions.httpOptions);
    }

    updateProduct(product, token: string) {
        let body = JSON.stringify(product);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.put(this.URL + 'products/' + product.id, body, httpOptions);
    }

    getProduct(product, token: string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Product>(this.URL + 'product/' + product, httpOptions);
    }

    deleteProduct(product, token:string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + 'products/' + product, httpOptions);
    }

    sendEmailBienvenida(loginObject){
        console.log(loginObject);
        let body = JSON.stringify(loginObject);
        return this.http.post(this.URL + 'products/' +'sendemailbienvenida', loginObject, HttpDefinitions.httpOptions);
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'http://localhost:4200',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}