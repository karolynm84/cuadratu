import {Injectable} from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { LoginObject } from "../../models/login-object";
import { Session } from "../../models/session";
import { HttpDefinitions } from "./http-definitions.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private URL = HttpDefinitions.URL;
  private accessToken = []; // variable to store the access token

  constructor(private http: HttpClient) {}

  login (loginObj: LoginObject): Observable<Session> {
    console.info("---login---");
    let body = JSON.stringify(loginObj);
    console.log(body);
    return this.http.post<Session>(this.URL + 'login', body, HttpDefinitions.httpOptionsJson);
  }

  logout(token:string): Observable<Boolean> {    
    return this.http.get<Boolean>(this.URL + 'logout', HttpDefinitions.getHttpOptions(token));
  }
  private extractData(res: Response) {
      let body = res.json();
      return body;
  }

  getToken() {
      return this.http.post(this.URL, HttpDefinitions.httpOptions);                        
  }
  
  setToken(token) {
  this.accessToken = token;  // save the access_token
  }
}
