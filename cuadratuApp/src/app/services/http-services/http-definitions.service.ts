import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpDefinitions {
static URL='http://cuadratu-api.us-east-2.elasticbeanstalk.com/api/';
static CLIENT_SECRET= '4uCdTJlM9aPtHaj2IIR6PWT3QZD0AtZ8HI9oQ92n';
static token;

constructor() {
  console.log('Hello HTTP Definitions Service');
}

static httpOptions = {
  headers: new HttpHeaders()
  .set('Content-Type', 'text/xml') 
  .append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS') 
  .append('Access-Control-Allow-Origin', '*')
  .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method")
, responseType:'text' as 'text'
};

static httpOptionsJson = {
  headers: new HttpHeaders({  'Content-Type': 'application/json', 
                              'Accept': 'application/json',
                              'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                              "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
                              'Access-Control-Allow-Origin':'http://localhost:4200',
                          })
};

static httpOptionsJsonTokenSafe = {
  headers: new HttpHeaders()
  .set('Content-Type', 'application/json') 
  .append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS') 
  .append('Access-Control-Allow-Origin', '*')
  .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method")
  .append('Accept', 'application/json')
  .append('client-id','1')
  .append('client_secret', HttpDefinitions.CLIENT_SECRET)
  , responseType:'text' as 'json'
};

public static getHttpOptions(token){
  console.log(HttpDefinitions.CLIENT_SECRET);

  return {    
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                              'Accept': 'application/json',
                              'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                              "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
                              'Access-Control-Allow-Origin':'http://localhost:4200',
                              'client-id':'1',
                              'client_secret':HttpDefinitions.CLIENT_SECRET,
                              'Authorization': 'Bearer ' + token
                            })   
  };
}

public static getHttpOptionsJsonTokenSafeParams(params) {
  return   {
    params: new HttpParams(params),
    headers: new HttpHeaders()
    .set('Content-Type', 'application/json') 
    .append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS') 
    .append('Access-Control-Allow-Origin', '*')
    .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method")
    .append('Accept', 'application/json')
    .append('client-id','1')
    .append('client_secret', HttpDefinitions.CLIENT_SECRET)
   ,responseType:'text' as 'json'
}
};

static headers = new HttpHeaders({'Content-Type': 'application/json', 
'Accept': 'application/json',
'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
"Access-Control-Allow-Headers": "Origin, X-Requested-With,  Access-Control-Allow-Origin, Content-Type, Accept, **Authorization**",
'Access-Control-Allow-Origin':'http://localhost:4200',
'responseType':'json'
});

static httpOptionsTokenSafe = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                              'Accept': 'application/json',
                              'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
                              "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
                              'Access-Control-Allow-Origin':'http://localhost:4200',
                              'client-id':'1',
                              'client_secret':HttpDefinitions.CLIENT_SECRET
                            })
};

}
