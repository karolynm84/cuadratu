import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpDefinitions } from './http-definitions.service';
import { Invitation } from 'src/app/models';
 
@Injectable()
export class InvitationService {
    invitations: Invitation[];

    private readonly URL = HttpDefinitions.URL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getInvitations(token):  Observable<Invitation[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Invitation[]>(this.URL+'invitations', httpOptions);                    
    }

     // Uses http.get() to load data from a single API endpoint
     getInvitationsByUser(token,id):  Observable<Invitation[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Invitation[]>(this.URL+'invitations/'+id, httpOptions);                    
    }

    createInvitation(invitation, token:string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        console.log(invitation);
        let body = JSON.stringify(invitation);
        return this.http.post(this.URL + 'invitation', body, httpOptions);
    }

    updateInvitation(invitation) {
        let body = JSON.stringify(invitation);
        return this.http.put(this.URL + 'invitations/' + invitation.id, body, HttpDefinitions.httpOptions);
    }

    /**Gets invitation by code */
    getInvitation(code) {
        return this.http.get<Invitation>(this.URL + 'invitation/' + code, HttpDefinitions.httpOptionsJson);
    }

    deleteInvitation(invitation, token:string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + 'invitations/' + invitation, httpOptions);
    }

    sendEmailInvitation(loginObject){
        console.log(loginObject);
        let body = JSON.stringify(loginObject);
        return this.http.post(this.URL + 'invitations/' +'sendemailinvitation', loginObject, HttpDefinitions.httpOptions);
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'http://localhost:4200, http://www.cuadratu.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}