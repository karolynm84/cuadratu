import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Place, Country, State, City } from 'src/app/models';
import { HttpDefinitions } from './http-definitions.service';
 
@Injectable()
export class PlaceService {
    places: Place[];

    private readonly URL = HttpDefinitions.URL;

    constructor(
        private http:HttpClient
    ) {}
 
    // Uses http.get() to load data from a single API endpoint
    getPlaces(token):  Observable<Place[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Place[]>(this.URL+'places', httpOptions);                    
    }
    getUserPlaces(id, token):  Observable<Place[]> {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Place[]>(this.URL+'userplaces/'+id, httpOptions);                    
    }

     // Uses http.get() to load data from a single API endpoint
     public getCountries():  Observable<Country[]> {
        /*const httpOptions = {
            headers:  this.getHeaders(token)
        };*/
        return this.http.get<Country[]>(this.URL+'countries', HttpDefinitions.httpOptionsTokenSafe);                    
    }
    // Uses http.get() to load data from a single API endpoint
    public getCities(state_id):  Observable<City[]> {
        /*const httpOptions = {
            headers:  this.getHeaders(token)
        };*/
        return this.http.get<City[]>(this.URL+'cities/'+state_id, HttpDefinitions.httpOptionsTokenSafe);                    
    }
    // Uses http.get() to load data from a single API endpoint
    public getStates(country_id):  Observable<State[]> {
        /*const httpOptions = {
            headers:  this.getHeaders(token)
        };*/
        return this.http.get<State[]>(this.URL+'states/'+country_id, HttpDefinitions.httpOptionsTokenSafe);                    
    }

    createPlace(place) {
        console.log(place);
        let body = JSON.stringify(place);
        return this.http.post(this.URL + 'signup', body, HttpDefinitions.httpOptionsTokenSafe);
    }

/*    createPlaceByInvitation(place) {
        console.log(place);
        let body = JSON.stringify(place);
        return this.http.post<Place>(this.URL +'place'+ '/store', body, HttpDefinitions.httpOptionsJson);
    }*/

    updatePlace(place, token: string) {
        let body = JSON.stringify(place);
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.put(this.URL + 'places/' + place.id, body, httpOptions);
    }

    getPlace(place, token: string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.get<Place>(this.URL + 'place/' + place, httpOptions);
    }

    deletePlace(place, token:string) {
        const httpOptions = {
            headers:  this.getHeaders(token)
        };
        return this.http.delete(this.URL + 'places/' + place, httpOptions);
    }

    public getHeaders (token: string) : HttpHeaders {
        return new HttpHeaders({ 'Content-Type': 'application/json', 
        'Accept': 'application/json',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, **Authorization**",
        'Access-Control-Allow-Origin':'http://localhost:4200, http://www.tnoradio.com',
        'client-id':'1',
        'client_secret':'Ctlu2veGArGCIrNUYwI7zW5SmyAefH1bZO4URDYh',
        'Authorization': 'Bearer ' + token
      })  
    }
}