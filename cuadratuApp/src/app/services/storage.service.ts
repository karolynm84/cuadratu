import { Injectable } from '@angular/core';
import { Session } from '../models/Session';
import { Router } from '@angular/router';
import { User, UserContacts, UserProfile, UserSettings } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private router: Router) { 
    console.log("storageService constructor()");
    this.localStorageService = localStorage;
    this.currentSession = this.loadSessionData();
  }

  private localStorageService: any;
  private currentSession : Session = null;

  loadSessionData(): Session{
    console.log("--loadSessionData---");
    var sessionStr = this.localStorageService.getItem('currentUser');
    return (sessionStr) ? <Session> JSON.parse(sessionStr) : null;
  }

  setCurrentSession(session: Session): void {
    console.log("---setCurrentSession---");    
    //console.log("--session--");
    //console.log(session);
    session.user.contacts = new UserContacts;
    session.user.profile = new UserProfile;
    session.user.settings = new UserSettings;
    session.user.profile.name = session.user.name;
    session.user.profile.surname = session.user.lastname;
    session.user.contacts.email = session.user.email;
    this.currentSession = session;
    this.localStorageService.setItem('currentUser', JSON.stringify(session));
    this.localStorageService.setItem('token',session.access_token);
  }
  getCurrentSession(): Session {
    console.log("---get Current Session---");
    var sessionStr = this.localStorageService.getItem('currentUser');
    return JSON.parse(sessionStr);
  }
  removeCurrentSession(): void {
    console.info("---removeCurrentSession---");
    this.localStorageService.removeItem('currentUser');
    this.currentSession = null;
    this.localStorageService.setItem('currentUser', null);
    this.localStorageService.removeItem('token');
  }
  getCurrentUser(): User {
    console.log("---getCurrentUser---");
    var sesion: Session = this.getCurrentSession();
    return (sesion && sesion.user) ? sesion.user : null;
  };
  isAuthenticated(): boolean {
    console.log("--isAuthenticated---");
    return (this.getCurrentToken() != null) ? true : false;
  };
  getCurrentToken(): string {
    console.log("---getCurrentToken---");
    var sesion = this.getCurrentSession();
    return (sesion && sesion.access_token) ? sesion.access_token : null;
  };

  logout(): void {
    console.log("--logout()--");
    this.removeCurrentSession();
    this.localStorageService.setItem('currentUser', null);
    this.router.navigate(['/']);
  }
}

