export class Invitation {    

    email: string;
    senders_mail: string;    
    user_id: number;
    sent_date:Date;
    code:string;
    status: boolean;
    
    constructor(
        email: string,
        senders_mail: string,
        user_id: number
        )
    {
        this.email= email;    
        this.senders_mail= senders_mail;
        this.user_id= user_id;
    }        
}