export class Place {
    id:number;   
    municipality: string;
    zip_code: string;    
    street: string;
    city_id:Date;
    address_1:string;
    address_2:string;         
}

export class Country {   
    id:number;
    name:string;        
}

export class State {   
    id:number;
    name:string; 
    country_id:number;       
}

export class City {   
    id:number;
    name:string;
    state_id:number;        
}