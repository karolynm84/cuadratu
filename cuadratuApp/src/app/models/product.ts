export class Product {
    id: number;
    name: string;
    price: number;
    description: string;
    aditional_info: string;
    user_id: number;
    existency_qty: number;
    condition: string;
    total_points: number;
    discount: number;
    reviews_avg: number;
    category_id: number;
}