import { Profile } from "selenium-webdriver/firefox";

export class User {
    public id: number;
    public name: string;
    public username: string;
    public lastname: string;
    public email: string;
    public password?: string;
    public password_confirmation?: string;
    public type_id: number;
    public user_id: number;
    public identification: string;
    public telephone: string;
    public birthdate: Date;
    public place_id: boolean;
    public subscribed: boolean;
    public state:boolean;
    public profile:UserProfile;
    public work: UserWork;
    public contacts: UserContacts;
    public social: UserSocial;
    public settings: UserSettings;
}

export class UserProfile {  
    name: string;
    surname: string;  
    birthday: Object;
    gender: string;
    image: string;
    identification: string;
  }
  
  export class UserWork {
    company: string;
    position: string;
    salary: number;
  }
  
  export class UserContacts{
    email: string;
    phone: string;
    address: string;  
  }
  
  export class UserSocial {
    facebook: string;
    twitter: string;
    google: string;
  }
  
  export class UserSettings{
    isActive: boolean;
    isDeleted: boolean;
    registrationDate: Date;
    joinedDate: Date;
  }